#include "loggerinterceptor.h"
#include "icontext.h"

//Instantiate logger variable
LoggerInterceptor::LoggerInterceptor(){
	logFile = new Logger();
	logFile->initLog();
	message << "Log initializing.\n";
}

//Add string from context to stringBuffer variable
void LoggerInterceptor::event_callback_1(IContext* context){
    message << context->getValue();
}

//Write stringBuffer variable to file and clear stringBuffer
void LoggerInterceptor::event_callback_2(){
	logFile->writeToLog(message.str());
	message.clear();
	message.str(string());
}
