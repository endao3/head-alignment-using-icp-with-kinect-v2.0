/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#pragma once
#ifndef IDISPATCHER_H
#define IDISPATCHER_H

class IContext;
class Interceptor;

//Interface for Dispathcer Objects
class IDispatcher {
public:
    virtual void dispatch(IContext*) = 0;
    virtual void registerInterceptor(Interceptor*) = 0;
    virtual void removeInterceptor(Interceptor*) = 0;
};

#endif // IDISPATCHER_H
