/*
2  * Software License Agreement (BSD License)
3  *
4  *  Point Cloud Library (PCL) - www.pointclouds.org
5  *  Copyright (c) 2010-2011, Willow Garage, Inc
6  *  Copyright (c) 2012-, Open Perception, Inc.
7  *
8  *  All rights reserved.
9  *
10  *  Redistribution and use in source and binary forms, with or without
11  *  modification, are permitted provided that the following conditions
12  *  are met:
13  *
14  *   * Redistributions of source code must retain the above copyright
15  *     notice, this list of conditions and the following disclaimer.
16  *   * Redistributions in binary form must reproduce the above
17  *     copyright notice, this list of conditions and the following
18  *     disclaimer in the documentation and/or other materials provided
19  *     with the distribution.
20  *   * Neither the name of the copyright holder(s) nor the names of its
21  *     contributors may be used to endorse or promote products derived
22  *     from this software without specific prior written permission.
23  *
24  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
25  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
26  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
27  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
28  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
29  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
30  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
31  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
32  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
33  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
34  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
35  *  POSSIBILITY OF SUCH DAMAGE.
36  *
37  * $Id$
38  *
39  */

/*
NOTE:
This cclass contains code from PCL 1.8 Tutorial on Interative ICP 
	which I refactored into Object Oriented format to interact with other objects in the program
		with features modified,added and removed where necessary. 
			Disclaimer above for Open Source reuse.
										
Reference
---------
Pointclouds.org. (2017). Documentation - Point Cloud Library (PCL). [online] 
Available at: http://pointclouds.org/documentation/tutorials/interactive_icp.php 
[Accessed 31 Mar. 2017].
*/

#include "Interactive_ICP.h"
#include "simplecontext.h"
#include "loggerdispatcher.h"
#include <regex>

/*
Constructor
	PointCloud_Features variable instantiated
		Intereceptor parameters setup
*/
Interactive_ICP::Interactive_ICP(Interceptor* interceptor) {
	target_Features = new PointCloud_Features("Target Cloud");
	source_Features = new PointCloud_Features("Source Cloud");
	icp_Features = new PointCloud_Features("ICP Cloud");

	context = new SimpleContext();
	dispatcher = new LoggerDispatcher();

	dispatcher->registerInterceptor(interceptor);
	message = "";
}

/**
Modified version of PCL 1.8 code to add the transformation matrix to the logfile
	Append transformation matrix to string for logFile
**/
void Interactive_ICP::print4x4Matrix(const Eigen::Matrix4d & matrix)
{
	message += ("Rotation matrix :\n");
	message += ("    | " + to_string(matrix(0, 0)) + " " + to_string(matrix(0, 1)) + " " + to_string(matrix(0, 2)) + " |\n");
	message += ("R = | " + to_string(matrix(1, 0)) + " " + to_string(matrix(1, 1)) + " " + to_string(matrix(1, 2)) + " |\n");
	message += ("    | " + to_string(matrix(2, 0)) + " " + to_string(matrix(2, 1)) + " " + to_string(matrix(2, 2)) + " |\n");
	message += ("Translation vector :\n");
	message += ("t = <" + to_string(matrix(0, 3)) + " " + to_string(matrix(1, 3)) + " " + to_string(matrix(2, 3)) + ">\n");
}

/**
Load PLY file into cloud variable
	NOT USED
int Interactive_ICP::loadPLYFile(string filename) {
	/*message += ("Loading PLY File: " + filename + "\n");
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud(new pcl::PointCloud<pcl::PointXYZRGB>());
	if (pcl::io::loadPLYFile(filename, *pointcloud) < 0)
	{

		PCL_ERROR("\nPLY cloud was not loaded from PLY File: %s", filename);
		message += ("Error loading PLY cloud from file:" + filename);
		context->setValue(message);
		dispatcher->dispatch(context);
		return (-1);
	}

	//target_Cloud_Normals = pointcloud;
	pointcloud = nullptr;

	message += ("Loaded file " + filename + " (" + to_string(target_Cloud_Normals->size()) + " points in " + to_string(timer.toc()) + " ms\n");
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";

	
	return 0;
}*/



/**
Load PCD file into cloud variable based on boolean
	If False then create Target Cloud-Normals for ICP, then set Keypoint features and Generate Mesh
		Else then create ICP and Source Cloud-Normals for ICP, then set Source Keypoint features
			Dispatch messages to Interceptor
**/
int Interactive_ICP::loadPCDFile(bool typeOfCloud, string filename) {
	message += ("\nLoading PCD File: " + filename + "\n");
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud(new pcl::PointCloud<pcl::PointXYZRGB>()); //Used to bypass initialization issues

	if (pcl::io::loadPCDFile(filename, *pointcloud) < 0)
	{
		PCL_ERROR("\nPCD cloud was not loaded from PCD File: %s", filename);
		message += ("Error loading PCD cloud from file:" + filename);
		context->setValue(message);
		dispatcher->dispatch(context);
		return (-1);
	}

	if (!typeOfCloud)
	{
		cout << "Step 1. Creating Target Cloud-Normals..."<<endl;
		target_Features->setCloudNormals(pointcloud);
		cout << "Step 2. Determining Target Keypoints..." << endl;
		target_Features->setKeyPointCapture(pointcloud);
		cout << "Step 3. Generating Target Mesh..." << endl;
		target_Features->setMesh(target_Features->getCloudNormals());
		//computeCloudNormals(pointcloud,false);
		//keyPointCapture(pointcloud, false);
		//generateMesh(target_Cloud_Normals, 1);
		message += ("ICP Target Cloud with Keypoints and Mesh loaded from file in command line.\n");
	}
	else
	{
		//Assign pointcloud from file to be used as input for transformation by ICP
		//fileCloud = true; //Cloud came from file and not from Kinect
		cout << "Step 1. Creating Source Cloud-Normals..." << endl;
		source_Features->setCloudNormals(pointcloud);
		cout << "Step 2. Determining Source Keypoints..." << endl;
		source_Features->setKeyPointCapture(pointcloud);

		//Fill ICP Cloud
		cout << "Step 3. Creating ICP Cloud-Normals..." << endl;
		icp_Features->setCloudNormals(pointcloud);
		//cout << "Generating Source Mesh" << endl;
		//source_Triangles_Mesh = pcd_Features->generateMesh(source_Cloud_Normals);
		//computeCloudNormals(pointcloud,true);
		//keyPointCapture(pointcloud, true);
		//generateMesh(source_Cloud_Normals, 2);Using ICP Cloud Mesh at the moment
		message += ("ICP Source Cloud with Keypoints loaded from file supplied by user.\n");
	}

	message += ("Loaded file " + filename + " (" + to_string(pointcloud->size()) + " points in " + to_string(timer.toc()) + " ms\n");
	pointcloud = nullptr;

	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";

	return 0;
}



/**
Setup the ICP Process
	Start timer
		Set Centroids
			Get Angle
				Create transformation matrix
					Setup ICP algorithm with PointCloud-Normals
						Apply first alignment
							Dispatch message to Interceptor
					
**/
int Interactive_ICP::setupICP()
{
	timer.tic();
	setCentroids();
	getAngle();

	// Defining a rotation matrix and translation vector
	transformation_matrix = Eigen::Matrix4d::Identity();

	// A rotation matrix (see https://en.wikipedia.org/wiki/Rotation_matrix)
	double theta = M_PI / 8;  // The angle of rotation in radians
	transformation_matrix(0, 0) = cos(theta);
	transformation_matrix(0, 1) = -sin(theta);
	transformation_matrix(1, 0) = sin(theta);
	transformation_matrix(1, 1) = cos(theta);

	// A translation on Z axis (0.4 meters)
	transformation_matrix(2, 3) = 0.4;

	message += "ICP Process initializing.\n";
	// The Iterative Closest Point algorithm
	timer.tic();

	//icp_cloud = source_cloud;
	//icp_Cloud_Normals
	//icp_test_Features->getCloudNormals() = source_Features->getCloudNormals();
	icp_Cloud_Normals = icp_Features->getCloudNormals();
	//icp_Cloud_Normals = source_Features->getCloudNormals();

	//Auto generated Iterations
	//regex numberInt("^([0-9]+\d*|0)$");
	regex numberInt("^[1-9][0-9]*$");
	do {
		cout << "\n--------------------------------"
			<< "\nEnter Auto Generated Iterations."
			<< "\n---------------------------------"
			<< "\nNumber:> ";
		getline(cin, input);
	} while (!regex_match(input, numberInt));
	iterations = stoi(input);

	icp.setMaximumIterations(iterations-1);//Auto generated iterations
	icp.setTransformationEpsilon(1e-8);// Stop ICP Align if difference between points is within 1*10^-8 (0.00000001)
	icp.setRANSACIterations(50);//Outlier removal
	icp.setRANSACOutlierRejectionThreshold(0.05);//Keep as standard

	/*icp.setInputSource(source_cloud);
	icp.setInputTarget(target_cloud);
	icp.setInputSource(source_Cloud_Normals);
	icp.setInputTarget(target_Cloud_Normals);*/
	icp.setInputSource(icp_Cloud_Normals);
	icp.setInputTarget(target_Features->getCloudNormals());

	
	message += "ICP Process aligning point clouds.\n";

	//icp.align(*icp_cloud);
	icp.align(*icp_Cloud_Normals);
	/*cout << "Generating ICP Cloud Mesh" << endl;
	message += "Generating ICP Cloud Mesh\n";
	generateMesh(icp_Cloud_Normals, 3);*/
	icp.setMaximumIterations(1);  // We set this variable to 1 for the next time we will call .align () function
	message += "Applied " + to_string(iterations) + " ICP iteration(s) in " + to_string(timer.toc()) + " ms\n";

	if (icp.hasConverged())
	{
		message += ("\nICP has converged, score is " + to_string(icp.getFitnessScore()) + "\n"
			+ "ICP transformation " + to_string(iterations) + " : cloud_in -> cloud_target\n");

		transformation_matrix = icp.getFinalTransformation().cast<double>();
		print4x4Matrix(transformation_matrix);
		getAngleOfRotation();
	}
	else
	{
		PCL_ERROR("\nICP has not converged.\n");
		message += ("\nICP has not converged.\n");
		context->setValue(message);
		dispatcher->dispatch(context);
		return (-1);
	}
	
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
	return 0;
}




/*
Initialize visualizer
	Create the viewports
		Add Pointclouds
			Add User Instructions
				Start Visualizer
					Wait for user choice
						Execute ICP iterations or Mesh creation
*/
int Interactive_ICP::visualizeICP()
{
	// Visualization
	pcl::visualization::PCLVisualizer viewer("ICP_FYP_Head Pose Estimation");
	// Create two verticaly separated viewports
	int v1(0);
	int v2(1);
	viewer.createViewPort(0.0, 0.0, 0.5, 1.0, v1);
	viewer.createViewPort(0.5, 0.0, 1.0, 1.0, v2);

	// Original point cloud is white
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBNormal> cloud_in_color_h(target_Features->getCloudNormals(), (int)255 * 1, (int)255 * 1,
		(int)255 * 1);
	viewer.addPointCloud(target_Features->getCloudNormals(), cloud_in_color_h, "cloud_in_v1", v1);
	viewer.addPointCloud(target_Features->getCloudNormals(), cloud_in_color_h, "cloud_in_v2", v2);

	// Kinect point cloud is green
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBNormal> cloud_tr_color_h(source_Features->getCloudNormals(), 0, 255, 0);
	viewer.addPointCloud(source_Features->getCloudNormals(), cloud_tr_color_h, "cloud_tr_v1", v1);

	// ICP aligned point cloud is yellow
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBNormal> cloud_icp_color_h(icp_Cloud_Normals, 255, 255, 0);
	viewer.addPointCloud(icp_Cloud_Normals, cloud_icp_color_h, "cloud_icp_v2", v2);

	
	//KeyPoints are blue for Target Cloud
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> cloud_key_color_target(target_Features->getKeyPointCapture(), 0, 0, 255);
	viewer.addPointCloud(target_Features->getKeyPointCapture(), cloud_key_color_target, "target_cloud_keypoints", v2);

	//viewer.addPolygonMesh(*source_Triangles_Mesh,"Source Mesh",v1);
	viewer.addPolygonMesh(*target_Features->getMesh(), "Target Mesh", v1);
	//viewer.addPolygonMesh(*icp_Triangles_Mesh, "ICP Mesh", v2);

	/*
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBNormal> cloud_keyNormal_color_target(target_Cloud_KeyPoints_Normals, 0, 0, 255);
	viewer.addPointCloud(target_Cloud_KeyPoints_Normals, cloud_keyNormal_color_target, "target_cloud_keypoints_normals", v1);
	*/
	/*
	VTK Mapper Bug
	for (int i = 0; i<source_Cloud_KeyPoints->size(); i++)
		viewer.addSphere(target_Cloud_KeyPoints->points[i], 0.1, 0.0, 0.0, 0.5, "target_sphere", v1);
		*/

	//KeyPoints are red for Source Cloud
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> cloud_key_color_source(source_Features->getKeyPointCapture(), 255, 0, 0);
	viewer.addPointCloud(source_Features->getKeyPointCapture(), cloud_key_color_source, "source_cloud_keypoints", v1);

	//pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBNormal> cloud_keyNormal_color_source(source_Cloud_KeyPoints_Normals, 255,0,0);
	//viewer.addPointCloud(source_Cloud_KeyPoints_Normals, cloud_keyNormal_color_source, "source_cloud_keypoints_normals", v1);
	
	/*
	VTK Mapper Bug 
	for(int i = 0; i<source_Cloud_KeyPoints->size();i++)
		viewer.addSphere(source_Cloud_KeyPoints->points[i], 0.1, 0.5, 0.0, 0.0, "source_sphere", v1);
		*/

	//Add Coordinate Axis size 25cm
	//target_Centroid = target_Features->getCentroids();
	viewer.addCoordinateSystem(0.25, target_Centroid.x() - 0.07, target_Centroid.y() - 0.1, target_Centroid.z(), "Axis", 0);

	// Adding text descriptions in Viewport v1
	viewer.addText("POINT CLOUD INFO\n---------------------------------------", 10, 125, 14, 1, 1, 1, "icp_info_header", v1);
	viewer.addText("White: Original point cloud", 10, 95, 13, 1, 1, 1, "icp_info_1", v1);
	viewer.addText("Blue:   Original Keypoints\n---------------------------------------------", 10, 55, 13, 0, 0, 1, "icp_keypoints_1", v1);
	viewer.addText("Green: Kinect Captured point cloud", 10, 35, 13, 0, 1, 0, "icp_info_2", v1);
	viewer.addText("Red:     Kinect Captured Keypoints", 10, 15, 13, 1, 0, 0, "icp_keypoints_2", v1);
	viewer.addText(headPos, 10, 700, 16, 1, 1, 1, "Head Pose: ", v1);
	viewer.setBackgroundColor(0, 0, 0, v1);

	//Adding text descriptions in Viewport v2
	//Add Point Cloud Data -> Bottom Left
	viewer.addText("POINT CLOUD INFO\n-----------------------------------", 10, 65, 14, 1, 1, 1, "icp_info_header2", v2);
	viewer.addText("White: Original point cloud", 10, 35, 13, 1, 1, 1, "icp_info_3", v2);
	viewer.addText("Yellow: ICP aligned point cloud", 10, 15, 13, 1, 1, 0, "icp_info_4", v2);
	//Add in Yaw,Pitch,Roll and Fitness Value -> Top-Right
	

	icp.align(*icp_Cloud_Normals);//Get latest fitness score
	viewer.addText("Fitness Score: " + to_string(icp.getFitnessScore()), 550, 775, 16, 1, 1, 1, "Fitness: ", v2);
	viewer.addText("Yaw   : " + to_string(yaw * 180 / PI), 550, 750, 16, 0, 0, 1, "Yaw: ", v2);
	viewer.addText("Pitch :  " + to_string(pitch * 180 / PI), 550, 725, 16, 0, 1, 0, "Pitch: ", v2);
	viewer.addText("Roll  :    " + to_string(roll * 180 / PI), 550, 700, 16, 1, 0, 0, "Roll: ", v2);
	//Add in instructions for ICP -> Top-Left
	viewer.addText("ICP Options\n-----------------------------", 10, 750, 14, 0, 1, 0, "icp_options", v2);
	viewer.addText("F1: 1 Iteration", 10, 725, 13, 1, 1, 1, "F1: 1 Iteration", v2);
	viewer.addText("F2: 5 Iterations", 10, 700, 13, 1, 1, 1, "Five Iter", v2);
	viewer.addText("F3: 10 Iterations", 10, 675, 13, 1, 1, 1, "Ten Iter", v2);
	viewer.addText("F4: 50 Iterations", 10, 650, 13, 1, 1, 1, "FiftyHundred Iter", v2);
	viewer.addText("F5: 100 Iterations", 10, 625, 13, 1, 1, 1, "OneHundred Iter", v2);
	viewer.addText("Insert Button: Create Mesh", 10, 600, 13, 0, 0, 1, "Mesh Creator", v2);
	viewer.addText("Alt +: Zoom In", 10, 575, 13, 0, 1, 0, "ZoomIn", v2);
	viewer.addText("Alt -: Zoom Out", 10, 550, 13, 0, 1, 0, "ZoomOut", v2);
	//viewer.addText("End Button: Close Window", 10, 575, 16, 1, 0, 0, "End_Window", v2);

	//Add in ICP Status for ICP -> Bottom Right
	viewer.addText("Status: ICP Stopped", 550, 40, 16, 1, 0, 0, "ICP_Status", v2);
	iterations_cnt = "ICP iterations = " + to_string(iterations);
	viewer.addText(iterations_cnt, 550, 15, 16, 0, 1, 0, "Iteration Number: ", v2);

	// Set background color
	viewer.setBackgroundColor(0, 0, 0, v2);

	// Set camera position and orientation
	viewer.setCameraPosition(0, 1, 5.71266, 0, 1, -0.256907, 0);
	viewer.setSize(1280, 1024);  // Visualiser window size

	int stop = 0;
	bool meshGen = false;
	// Display the visualiser
	while (!viewer.wasStopped())
	{
		viewer.spinOnce();
		viewer.updateText("Fitness Score: " + to_string(icp.getFitnessScore()), 550, 775, 16, 1, 1, 1, "Fitness: ");
		// The user pressed "Tab" :
		if (GetKeyState(VK_F1) < 0){
			stop = 1;
			viewer.updateText("Status: ICP Registering...", 550, 40, 16, 0, 1, 0, "ICP_Status");
		}
		
		if (GetKeyState(VK_F2) < 0){
			stop = 5;
			viewer.updateText("Status: ICP Registering...", 550, 40, 16, 0, 1, 0, "ICP_Status");
		}

		if (GetKeyState(VK_F3) < 0){
			stop = 10;
			viewer.updateText("Status: ICP Registering...", 550, 40, 16, 0, 1, 0, "ICP_Status");
		}

		if (GetKeyState(VK_F4) < 0){
			stop = 50;
			viewer.updateText("Status: ICP Registering...", 550, 40, 16, 0, 1, 0, "ICP_Status");
		}

		if (GetKeyState(VK_F5) < 0) {
			stop = 100;
			viewer.updateText("Status: ICP Registering...", 550, 40, 16, 0, 1, 0, "ICP_Status");
		}
			

		/*if (GetKeyState(VK_END) < 0)
			viewer.close();*/

		//Polygon Mesh for ICP
		if (GetKeyState(VK_INSERT) < 0 && !meshGen){
			cout << "Status: Generating Mesh..." << endl;
			viewer.updateText("Status: Generating Mesh...", 550, 40, 16, 0, 0, 1, "ICP_Status");
			timer.tic();
			//Update Mesh for ICP Cloud
			icp_Features->setMesh(icp_Cloud_Normals);
			viewer.addPolygonMesh(*icp_Features->getMesh(), "ICP Mesh", v2);
			std::cout << "Generated ICP Mesh in " << timer.toc() << " ms" << std::endl;
			message += ("\nGenerated ICP Mesh in  " + to_string(timer.toc()) + " ms\n");
			meshGen = true;
		}

		while (stop > 0)
		{
			viewer.spinOnce();

			//Remove any previous ICP mesh before ICP process
			if (meshGen){
				viewer.removePolygonMesh("ICP Mesh");
				meshGen = false;
			}

			// The Iterative Closest Point algorithm
			timer.tic();
			//icp.align(*icp_cloud);
			icp.align(*icp_Cloud_Normals);

			std::cout << "Applied 1 ICP iteration in " << timer.toc() << " ms" << std::endl;
			message += ("\nApplied 1 ICP iteration in " + to_string(timer.toc()) + " ms\n");

			if (icp.hasConverged())
			{
				printf("\033[11A");  // Go up 11 lines in terminal output.
				printf("\nICP has converged, score is %+.0e\n", icp.getFitnessScore());
				std::cout << "\nICP transformation " << ++iterations << " : cloud_icp -> target_Cloud" << std::endl;

				message += ("ICP has converged, fitness score is " + to_string(icp.getFitnessScore())
					+ "\nICP transformation " + to_string(iterations) + " : cloud_icp -> target_Cloud.\n");

				transformation_matrix *= icp.getFinalTransformation().cast<double>();  // WARNING /!\ This is not accurate! For "educational" purpose only!
				print4x4Matrix(transformation_matrix);  // Print the transformation between original pose and current pose
				getAngleOfRotation();
				//viewer.updateText(headPos, 10, 0, 16, 1, 1, 1, "Head Pose: ");
				viewer.updateText("Fitness Score: " + to_string(icp.getFitnessScore()), 550, 775, 16, 1, 1, 1, "Fitness: ");
				viewer.updateText("Yaw   : " + to_string(yaw * 180 / PI), 550, 750, 16, 0, 0, 1, "Yaw: ");
				viewer.updateText("Pitch  : " + to_string(pitch * 180 / PI), 550, 725, 16, 0, 1, 0, "Pitch: ");
				viewer.updateText("Roll     : " + to_string(roll * 180 / PI), 550, 700, 16, 1, 0, 0, "Roll: ");

				iterations_cnt = "ICP iterations = " + to_string(iterations);// ss.str();
				viewer.updateText(iterations_cnt, 550, 15, 16, 0, 1, 0, "Iteration Number: ");

				viewer.updatePointCloud(icp_Cloud_Normals, cloud_icp_color_h, "cloud_icp_v2");
				//generateMesh(icp_Cloud_Normals, 3);
				//viewer.updatePolygonMesh(*icp_Triangles_Mesh,"ICP Mesh"); //Too slow to do on every iteration
				//viewer.updatePointCloud(icp_cloud, cloud_icp_color_h, "cloud_icp_v2");
				stop--;
			}

			else
			{
				PCL_ERROR("\nICP has not converged.\n");
				message += ("\nICP has not converged.\n");
				context->setValue(message);
				dispatcher->dispatch(context);
				_sleep(2000);
				exit(10);
			}
		}
		viewer.updateText("Status: ICP Stopped", 550, 40, 16, 1, 0, 0, "ICP_Status");
		//}
		//next_iteration = false;
	}
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
	return (0);
}



/*
Determine angle between Centroids of each PointCloud
FAILING: Getting 4 degrees ?
*/
void Interactive_ICP::getAngle() {
	/*
	int size = icp_Cloud->size();

	double x1 = target_Cloud_Normals_Normals->at(0).x;
	double y1 = target_Cloud_Normals_Normals->at(0).y;
	double z1 = target_Cloud_Normals_Normals->at(0).z;

	double x2 = icp_Cloud->at(0).x;
	double y2 = icp_Cloud->at(0).y;
	double z2 = icp_Cloud->at(0).z;

	double dot = x1*x2 + y1*y2 + z1*z2;
	double lenSq1 = x1*x1 + y1*y1 + z1*z1;
	double lenSq2 = x2*x2 + y2*y2 + z2*z2;
	double angle = acos(dot / sqrt(lenSq1 * lenSq2)) * 180.0 / PI;

	x2 = icp_Cloud->at(size/2).x;
	y2 = icp_Cloud->at(size / 2).y;
	z2 = icp_Cloud->at(size / 2).z;

	dot = x1*x2 + y1*y2 + z1*z2;
	lenSq1 = x1*x1 + y1*y1 + z1*z1;
	lenSq2 = x2*x2 + y2*y2 + z2*z2;
	angle += acos(dot / sqrt(lenSq1 * lenSq2)) * 180.0 / PI;

	x2 = icp_Cloud->at(size - 1).x;
	y2 = icp_Cloud->at(size - 1).y;
	z2 = icp_Cloud->at(size - 1).z;

	dot = x1*x2 + y1*y2 + z1*z2;
	lenSq1 = x1*x1 + y1*y1 + z1*z1;
	lenSq2 = x2*x2 + y2*y2 + z2*z2;
	angle += acos(dot / sqrt(lenSq1 * lenSq2)) * 180.0 / PI;

	/*
	double x2 = icp_Cloud->at(val).x;
	double y2 = icp_Cloud->at(val).y;
	double z2 = icp_Cloud->at(val).z;

	double dot = x1*x2 + y1*y2 + z1*z2;
	double lenSq1 = x1*x1 + y1*y1 + z1*z1;
	double lenSq2 = x2*x2 + y2*y2 + z2*z2;
	double angle = acos(dot / sqrt(lenSq1 * lenSq2)) * 180.0 / PI;*/

	//cout << "\nAngle: " << angle/3 << endl;

	double x1 = target_Centroid.x();
	double y1 = target_Centroid.y();
	double z1 = target_Centroid.z();
	//cout << "\nCentroid value1: " << x1 << ":" << y1 << ":" << z1 << endl;

	double x2 = source_Centroid.x();
	double y2 = source_Centroid.y();
	double z2 = source_Centroid.z();
	//cout << "\nCentroid value2: " << x2 << ":" << y2 << ":" << z2 << endl;

	/*double dotProduct = x1*x2 + y1*y2 + z1*z2;
	double lenSq1 = x1*x1 + y1*y1 + z1*z1;
	double lenSq2 = x2*x2 + y2*y2 + z2*z2;

	cout << dotProduct << ":" << lenSq1 << ":" << lenSq2 << endl;
	double angleRadian = acos(dotProduct / sqrt(lenSq1 * lenSq2));
	double angle = acos(dotProduct / sqrt(lenSq1 * lenSq2)) * 180.0 / PI;

	cout << "\nAngle: " + to_string(angle) << "\n" << endl;

	message += "\n___________\nTarget Centroid(x1,y1,z1): "+to_string(x1) + ":" + to_string(y1) + ":" + to_string(z1)+"\n";
	message += "Input Centroid(x2,y2,z2): "+to_string(x2) + ":" + to_string(y2) + ":" + to_string(z2) + "\n";
	message += "Dot Product: "+to_string(dotProduct) + "\nLenght of (x1,y1,z1): " + to_string(lenSq1)+ "\nLenght of (x2,y2,z2):" + to_string(lenSq2)+"\n";
	message += "\nAngle Degree:" + to_string(angle) + "\nAngle Radian:" + to_string(angleRadian)+"\n______________\n";

	double angle2 = atan2(x1 - x2, y1 - y2);
	cout << "\n\Angle2 Radian:" + to_string(angle2);
	cout << "\nAngle2 Degree:" + to_string(angle2*180 /PI) << endl;*/

	//cout << "Vector: <" << (x1 - x2) << "," << (y1 - y2) << "," << (z1 - z2) << ">" << endl;
	message += "Vector: <" + to_string(x1 - x2) + "," + to_string(y1 - y2) + "," + to_string(z1 - z2) + ">\n";
}



/**
Get yaw(Z), pitch(Y) and roll(X) from rotation matrix (Affine transformation)
	Use this data to determine angles and then head position
		Counter-Clockwise Rotation is positive radian/degree 
			(East -> West for Roll)  - roll = atan2 (t (2, 1), t (2, 2));
			(South -> North for Pitch) - pitch = asin (-t (2, 0));
			(Forward -> Backward for Yaw) - yaw = atan2 (t (1, 0), t (0, 0));
				Clockwise Rotation is negative radian/degree (Left -> Right)
**/
void Interactive_ICP::getAngleOfRotation() {
	rotation_matrix(0, 0) = transformation_matrix(0, 0);
	rotation_matrix(0, 1) = transformation_matrix(0, 1);
	rotation_matrix(0, 2) = transformation_matrix(0, 2);
	rotation_matrix(1, 0) = transformation_matrix(1, 0);
	rotation_matrix(1, 0) = transformation_matrix(1, 0);
	rotation_matrix(1, 1) = transformation_matrix(1, 1);
	rotation_matrix(1, 2) = transformation_matrix(1, 2);
	rotation_matrix(2, 0) = transformation_matrix(2, 0);
	rotation_matrix(2, 1) = transformation_matrix(2, 1);
	rotation_matrix(2, 2) = transformation_matrix(2, 2);

	headPos = "Head Pose\n--------------------------------";
	pcl::getEulerAngles(rotation_matrix, yaw, pitch, roll);
	if (yaw > 0)
		headPos += "\nSlope:                 Leaning Forward";
	else
		headPos += "\nSlope:                 Leaning Backward";

	if (pitch > 0)
		headPos += "\nOrientation:      Facing Up";
	else
		headPos += "\nOrientation:      Facing Down";

	if (roll > 0)
		headPos += "\nAngle:                Right of Centre";
	else
		headPos += "\nAngle:                Left of Centre";

	cout << "\nDEGREES\n---------\nYaw: " << (yaw*180.0 / PI) << "\nPitch: " << (pitch*180.0 / PI) << "\nRoll: " << (roll*180.0 / PI) << "\n-----------\n" << endl;
	message += "\nDEGREES\n---------\nYaw: " + to_string(yaw*180.0 / PI) + "\nPitch: " + to_string(pitch*180.0 / PI) + "\nRoll: " + to_string(roll*180.0 / PI) + "\n------------\n";

	context->setValue(message);
}



/*
Compute Centroids for Target and Source Clouds / Cloud Normals
*/
void Interactive_ICP::setCentroids() {
	
	//pcl::compute3DCentroid(*target_cloud, target_Centroid);
	//pcl::compute3DCentroid(*source_cloud, input_Centroid);
	
	pcl::compute3DCentroid(*target_Features->getCloudNormals(), target_Centroid);
	pcl::compute3DCentroid(*source_Features->getCloudNormals(), source_Centroid);
}
