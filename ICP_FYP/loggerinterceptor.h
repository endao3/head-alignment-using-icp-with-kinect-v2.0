/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#ifndef LOGGERINTERCEPTOR_H
#define LOGGERINTERCEPTOR_H
#include "interceptor.h"
#include "Logger.h"

using namespace std;

class IContext;
class LoggerInterceptor: public Interceptor
{
private:
	Logger* logFile;
	stringstream message;

public:
	LoggerInterceptor();
	virtual ~LoggerInterceptor() {}
    virtual void event_callback_1(IContext*);
    virtual void event_callback_2();
};

#endif // LOGGERINTERCEPTOR_H
