#include "Kinect_DataCapture.h"
#include "simplecontext.h"
#include "loggerdispatcher.h"

/*
Initialize required variables for Interceptor
*/
Kinect_DataCapture::Kinect_DataCapture(Interceptor* interceptor) {
	context = new SimpleContext();
	dispatcher = new LoggerDispatcher();

	dispatcher->registerInterceptor(interceptor);
	message = "";
}



/**
Instructions used from video tutorial https://www.youtube.com/watch?v=L1Kgm4S8c90
	Code refactored from https://gist.github.com/UnaNancyOwen/8f74fe7f4ee91827734d 
Reference
---------
Mvp.microsoft.com. (2017). Tsukasa Sugiura. [online] Available at: 
https://mvp.microsoft.com/en-us/PublicProfile/5000954?fullName=Tsukasa%20Sugiura 
[Accessed 31 Mar. 2017].

Initialize Kinect sensor if found
	Open Sensor if found
		Initialize Coordinate Mapper if found
			Dispatch messages to Logfile interceptor
**/
int Kinect_DataCapture::initSensor()
{
	// Create Sensor Instance
	hResult = GetDefaultKinectSensor(&pSensor);

	if (FAILED(hResult)) {
		cout << "Error: Kinect Sensor not found" << endl;
		message += ("Error : GetDefaultKinectSensor\n");
		context->setValue(message);
		dispatcher->dispatch(context);
		_sleep(2000);
		exit(1);
	}
	message += ("Kinect Sensor found.\n");

	// Open Sensor
	hResult = pSensor->Open();
	if (FAILED(hResult)) {
		cout << "Error: Kinect Sensor could not be opened." << endl;
		message += ("Error : IKinectSensor::Open()\n");
		context->setValue(message);
		dispatcher->dispatch(context);
		_sleep(2000);
		exit(1);
	}
	message += ("Kinect Sensor opened.\n");

	// Retrieve Coordinate Mapper
	hResult = pSensor->get_CoordinateMapper(&pCoordinateMapper);
	if (FAILED(hResult)) {
		cout << "Error: Coordinate Mapper was not found." << endl;
		message += ("Error : IKinectSensor::get_CoordinateMapper()\n");
		context->setValue(message);
		dispatcher->dispatch(context);
		_sleep(2000);
		exit(1);;
	}
	message += ("Coordinate Mapper found.\n");
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
	return 0;
}



/**
Instructions used from video tutorial https://www.youtube.com/watch?v=L1Kgm4S8c90
	Code refactored from https://gist.github.com/UnaNancyOwen/8f74fe7f4ee91827734d
Reference
---------
Mvp.microsoft.com. (2017).Tsukasa Sugiura.[online] Available at :
https://mvp.microsoft.com/en-us/PublicProfile/5000954?fullName=Tsukasa%20Sugiura 
[Accessed 31 Mar. 2017].

Initialize Colour sensor if found
	Open Sensor if found
			Release Sensor pointer
				Dispatch messages to Logfile interceptor
**/
int Kinect_DataCapture::colorCapture(int iteration)
{
	message += ("\n------------------------------------------\nIterations Left: " + to_string(iteration) + "\nColour capture started.\n");
	// Retrieved Color Frame Source
	hResult = pSensor->get_ColorFrameSource(&pColorSource);
	if (FAILED(hResult)) {
		message += ("Error : IKinectSensor::get_ColorFrameSource()\n");
		context->setValue(message);
		dispatcher->dispatch(context);
		return -1;
	}

	// Open Color Frame Reader
	hResult = pColorSource->OpenReader(&pColorReader);
	if (FAILED(hResult)) {
		message += ("Error : IColorFrameSource::OpenReader()\n");
		context->setValue(message);
		dispatcher->dispatch(context);
		return -1;
	}

	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
	SafeRelease(pColorSource);
	return 0;
}



/*
Instructions used from video tutorial https://www.youtube.com/watch?v=L1Kgm4S8c90
	Code refactored from https://gist.github.com/UnaNancyOwen/8f74fe7f4ee91827734d
Profile Reference
-----------------
Mvp.microsoft.com. (2017).Tsukasa Sugiura.[online] Available at :
https://mvp.microsoft.com/en-us/PublicProfile/5000954?fullName=Tsukasa%20Sugiura 
[Accessed 31 Mar. 2017].

Initialize Depth sensor if found
	Open Sensor if found
			Release Sensor pointer
				Dispatch messages to Logfile interceptor
*/
int Kinect_DataCapture::depthCapture()
{
	message += ("Depth capture started.\n");
	// Retrieved Depth Frame Source
	hResult = pSensor->get_DepthFrameSource(&pDepthSource);
	if (FAILED(hResult)) {
		message += ("Error : IKinectSensor::get_DepthFrameSource()\n");
		context->setValue(message);
		dispatcher->dispatch(context);
		return -1;
	}

	// Open Depth Frame Reader
	hResult = pDepthSource->OpenReader(&pDepthReader);
	if (FAILED(hResult)) {
		message += ("Error : IDepthFrameSource::OpenReader()\n");
		context->setValue(message);
		dispatcher->dispatch(context);
		return -1;
	}

	SafeRelease(pDepthSource);
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
	return 0;
}



/*
My own code based on the same principles as above
Used to capture body features

Initialize Body sensor if found
		Open Sensor if found
				Create CameraSpacePoint for Face capture
					Release Sensor pointer
						Dispatch messages to Logfile interceptor
*/
int Kinect_DataCapture::bodyCapture()
{
	message += "Body Tracking started.\n";

	hResult = pSensor->get_BodyFrameSource(&pBodySource);
	// Retrieved Body Frame Source
	if (FAILED(hResult)) {
		message += ("Error : IKinectSensor::get_BodyFrameSource()\n");
		context->setValue(message);
		dispatcher->dispatch(context);
		return -1;
	}

	// Open Body Frame Reader
	hResult = pBodySource->OpenReader(&pBodyReader);
	if (FAILED(hResult)) {
		message += ("Error : IBodyFrameSource::OpenReader()\n");
		context->setValue(message);
		dispatcher->dispatch(context);
		return -1;
	}

	faceSpacePoint = new CameraSpacePoint();// Initialize here to get head position later in Body Points array
	//Don`t progress program until bodt points are found. 

	SafeRelease(pBodySource);
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
	return 0;
}



/**
Code used from https://gist.github.com/UnaNancyOwen/8f74fe7f4ee91827734d 
Reference
---------
Mvp.microsoft.com. (2017). Tsukasa Sugiura. [online] Available at:
https://mvp.microsoft.com/en-us/PublicProfile/5000954?fullName=Tsukasa%20Sugiura
[Accessed 31 Mar. 2017].

Check if sensor is still found
		Set buffers to screen size
			Capture Data and assign to buffers
				Release Sensor pointers
					Dispatch messages to Logfile interceptor
**/
int Kinect_DataCapture::getLatestData()
{
	hResult = GetDefaultKinectSensor(&pSensor);
	if (FAILED(hResult))
	{
		cout << "Kinect sensor not found..." << endl;
		_sleep(2000);
		return -1;
	}
	depthBuffer.resize(SCR_WIDTH_DEPTH * SCR_HEIGHT_DEPTH);
	colorBuffer.resize(SCR_WIDTH_COLOR * SCR_HEIGHT_COLOR);

	// Acquire Latest Color Frame
	hResult = pColorReader->AcquireLatestFrame(&pColorFrame);
	if (SUCCEEDED(hResult)) {
		// Retrieved Color Data
		hResult = pColorFrame->CopyConvertedFrameDataToArray(colorBuffer.size() * sizeof(RGBQUAD), reinterpret_cast<BYTE*>(&colorBuffer[0]), ColorImageFormat::ColorImageFormat_Bgra);
		if (FAILED(hResult)) {
			message += ("Error : IColorFrame::CopyConvertedFrameDataToArray()\n");
			context->setValue(message);
			dispatcher->dispatch(context);
			return(-1);
		}
	}
	SafeRelease(pColorFrame);
	message += ("Colour Capture complete.\n");

	// Acquire Latest Depth Frame
	hResult = pDepthReader->AcquireLatestFrame(&pDepthFrame);
	if (SUCCEEDED(hResult)) {
		// Retrieved Depth Data
		hResult = pDepthFrame->CopyFrameDataToArray(depthBuffer.size(), &depthBuffer[0]);//reinterpret_cast<UINT16*>(&depthBuffer[0]));
		if (FAILED(hResult)) {
			message += "Error : IDepthFrame::CopyFrameDataToArray()\n";
			context->setValue(message);
			dispatcher->dispatch(context);
			return(-1);
		}
	}
	SafeRelease(pDepthFrame);
	message += ("Depth capture complete.\n");

	/**
	Acquire Latest Body Tracking Frame
	Code used from https://gist.github.com/Furkanzmc/2925d8b53a5002d6f526
	Reference
	---------
	�z�mc�, F. (2017). Furkanzmc (Furkan �z�mc�). [online] GitHub. 
	Available at: https://github.com/Furkanzmc 
	[Accessed 31 Mar. 2017].

	Capture Body Joints from Kiect Joint Sensor
		Put into array and send to processBodies method
			Release all Joint pointers in array
	**/
	hResult = pBodyReader->AcquireLatestFrame(&pBodyFrame);

	if (SUCCEEDED(hResult)) {
		IBody *bodies[BODY_COUNT] = { 0 };
		hResult = pBodyFrame->GetAndRefreshBodyData(_countof(bodies), bodies);

		if (SUCCEEDED(hResult)) {
			processBodies(BODY_COUNT, bodies);
			//After body processing is done, we're done with our bodies so release them.
			for (unsigned int bodyIndex = 0; bodyIndex < _countof(bodies); bodyIndex++) {
				SafeRelease(bodies[bodyIndex]);
			}

			SafeRelease(pBodyFrame);
			message += ("Body Tracking complete.\n");
		}
	}
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";

	return 0;
}



/**
Method to start point cloud creation
	If head capture then faceSpacePoint will have data and boolean sent to pointcloud creator is True
	 Else it is empty and boolean is False
		Draw returned pointcloud
			Dispatch messages to Logfile interceptor
**/
void Kinect_DataCapture::createPointCloud(PointCloud_Creator* pointCloudCreator, bool headCapture) {
	message += "Point Cloud creation starting\n";
	if (headCapture)
		pointcloud = pointCloudCreator->pointCloudInitialize(pCoordinateMapper, colorBuffer, depthBuffer, *faceSpacePoint, true);

	else
		pointcloud = pointCloudCreator->pointCloudInitialize(pCoordinateMapper, colorBuffer, depthBuffer, *faceSpacePoint, false);

	pointCloudCreator->draw_cloud(pointcloud);
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
}



/**
Code used from https://gist.github.com/Furkanzmc/2925d8b53a5002d6f526 
Reference
-------- -
�z�mc�, F. (2017).Furkanzmc(Furkan �z�mc�).[online] GitHub.
Available at : https://github.com/Furkanzmc 
[Accessed 31 Mar. 2017].

FAILS ON FIRST DATA CAPTURE EVERYTIME
	Create an array of features if body found
		Capture head joint
			Set camera space point for head position
**/
void Kinect_DataCapture::processBodies(const unsigned int &bodyCount, IBody **bodies)
{
	message += ("Body:" + bodyCount);
	for (unsigned int bodyIndex = 0; bodyIndex < bodyCount; bodyIndex++)
	{
		IBody *body = bodies[bodyIndex];

		//Get the tracking status for the body, if it's not tracked we'll skip it
		BOOLEAN isTracked = false;
		hResult = body->get_IsTracked(&isTracked);

		if (SUCCEEDED(hResult))
		{
			//If we're here the body is tracked so lets get the joint properties for this skeleton
			Joint joints[JointType_Count];
			hResult = body->GetJoints(_countof(joints), joints);

			if (SUCCEEDED(hResult)) {
				*faceSpacePoint = joints[JointType_Head].Position;

				if (faceSpacePoint->X + faceSpacePoint->Y + faceSpacePoint->Z) //Value was found
					break;
			}
		}
	}
}



//Return pointcloud
pcl::PointCloud<pcl::PointXYZRGB>::Ptr Kinect_DataCapture::getPointCloud() {
	return pointcloud;
}



//Return head position
double Kinect_DataCapture::getFacePos() {
	return faceSpacePoint->X + faceSpacePoint->Y + faceSpacePoint->Z;
}



/**
Instructions used from video tutorial https://www.youtube.com/watch?v=L1Kgm4S8c90
	Code refactored from https://gist.github.com/UnaNancyOwen/8f74fe7f4ee91827734d 
Reference
---------
Mvp.microsoft.com. (2017).Tsukasa Sugiura.[online] Available at :
https://mvp.microsoft.com/en-us/PublicProfile/5000954?fullName=Tsukasa%20Sugiura
[Accessed 31 Mar. 2017].

Release all pointer memory currently allocated
**/
void Kinect_DataCapture::shutdownSystem()
{
	// End Processing
	faceSpacePoint = nullptr;
	SafeRelease(pColorReader);
	SafeRelease(pDepthReader);
	SafeRelease(pBodyReader);
	SafeRelease(bodyReader);
	SafeRelease(pCoordinateMapper);
	if (pSensor) {
		pSensor->Close();
	}
	SafeRelease(pSensor);
	message += ("Kinect memory release successful.\n");
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
}


