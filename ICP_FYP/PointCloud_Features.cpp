/*
2  * Software License Agreement (BSD License)
3  *
4  *  Point Cloud Library (PCL) - www.pointclouds.org
5  *  Copyright (c) 2010-2011, Willow Garage, Inc
6  *  Copyright (c) 2012-, Open Perception, Inc.
7  *
8  *  All rights reserved.
9  *
10  *  Redistribution and use in source and binary forms, with or without
11  *  modification, are permitted provided that the following conditions
12  *  are met:
13  *
14  *   * Redistributions of source code must retain the above copyright
15  *     notice, this list of conditions and the following disclaimer.
16  *   * Redistributions in binary form must reproduce the above
17  *     copyright notice, this list of conditions and the following
18  *     disclaimer in the documentation and/or other materials provided
19  *     with the distribution.
20  *   * Neither the name of the copyright holder(s) nor the names of its
21  *     contributors may be used to endorse or promote products derived
22  *     from this software without specific prior written permission.
23  *
24  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
25  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
26  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
27  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
28  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
29  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
30  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
31  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
32  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
33  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
34  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
35  *  POSSIBILITY OF SUCH DAMAGE.
36  *
37  * $Id$
38  *
39  */


/*
NOTE:
This class contains code from PCL 1.8 Tutorials noted above the method defintions
	which I refactored into Object Oriented format to interact with other objects in the program
		with features modified,added and removed where necessary.
			Disclaimer above for Open Source reuse.

*/
#include "PointCloud_Features.h"



/*
Constructor: Set Name
*/
PointCloud_Features::PointCloud_Features(string name) {
	this->name = name;
}



//Return cloud Name
string PointCloud_Features::getCloudName() {
	return name;
}




/**
Code based on tutorial from ICP 1.8 on Estimating Surface Normals and OtherLab github example

Pointclouds.org. (2017). Documentation - Point Cloud Library (PCL). [online] 
Available at: http://pointclouds.org/documentation/tutorials/normal_estimation.php 
[Accessed 1 Apr. 2017].

GitHub. (2017). otherlab/pcl. [online]
Available at: https://github.com/otherlab/pcl/blob/master/examples/keypoints/example_sift_keypoint_estimation.cpp
[Accessed 1 Apr. 2017].

Method takes a point cloud ptr for conversion to a Normal Point Cloud
	Boolean source is true when data is either loaded from Kinect or file
		Source is false if it is the target point cloud loaded from the command line
			Kd-Tree is setup for Nearest Neighbour search in Normalization process
**/
void PointCloud_Features::setCloudNormals(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud) {
	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGB>());
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloudNormals(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
	normals.setSearchMethod(tree);
	//normals.setRadiusSearch(0.03);
	normals.setKSearch(50);
	normals.setInputCloud(pointcloud);
	normals.compute(*cloudNormals);

	for (int i = 0; i<cloudNormals->points.size(); ++i)
	{
		cloudNormals->points[i].x = pointcloud->points[i].x;
		cloudNormals->points[i].y = pointcloud->points[i].y;
		cloudNormals->points[i].z = pointcloud->points[i].z;
		cloudNormals->points[i].rgb = pointcloud->points[i].rgb;
	}
	input_Cloud_Normals = cloudNormals;
	tree = nullptr;
/*
	//Fill normal Target cloud
	if (!source)
	{
		cout << "Capturing Normals from Target cloud\n" << endl;
		target_Cloud_Normals = cloudNormals;
		message += "Target Cloud Normals added.\n";
	}

	//Fill normal Source cloud
	else
	{
		cout << "Capturing Normals from Source cloud\n" << endl;
		source_Cloud_Normals = cloudNormals;
		message += "Source Cloud Normals added.\n";
	}

	cloudNormals = nullptr;*//*
	tree = nullptr;
	input_Cloud_Normals = cloudNormals;
	cloudNormals = nullptr;
	return input_Cloud_Normals;*/
}



//Return pointcloud normals
pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr PointCloud_Features::getCloudNormals() {
	return input_Cloud_Normals;
}



/**
Code refactored from
GitHub. (2017). otherlab/pcl. [online] 
Available at: https://github.com/otherlab/pcl/blob/master/examples/keypoints/example_sift_normal_keypoint_estimation.cpp 
[Accessed 3 Apr. 2017].

Set KeyPoint Capture variables : Need to Optimize for suitability based on scale of point cloud
	Kd-Tree is setup for Nearest Neighbour search in SIFT (Scale Invariant Feature Transform) process
		Execute SIFT process looking for key edge features for future facial recognition procedures
			Copy to Keypoint variable
**/
void PointCloud_Features::setKeyPointCapture(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud) {
	// Parameters for sift computation: Left as example standard for now - Requires modification
	const float min_scale = 0.01f;
	const int n_octaves = 3;
	const int n_scales_per_octave = 4;
	const float min_contrast = 0.001f;

	// Estimate the sift interest points using normals values from xyz as the Intensity variants
	pcl::SIFTKeypoint<pcl::PointXYZRGB, pcl::PointWithScale>::Ptr sift(new pcl::SIFTKeypoint<pcl::PointXYZRGB, pcl::PointWithScale>);
	pcl::PointCloud<pcl::PointWithScale>::Ptr result(new pcl::PointCloud<pcl::PointWithScale>);;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_KeyPoints(new pcl::PointCloud<pcl::PointXYZRGB>);
	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGB>());

	sift->setSearchMethod(tree);
	sift->setScales(min_scale, n_octaves, n_scales_per_octave);
	sift->setMinimumContrast(min_contrast);
	sift->setInputCloud(pointcloud);
	sift->compute(*result);
	pcl::copyPointCloud(*result, *cloud_KeyPoints);
	/*if (!source)
	{
		cout << "Capturing Keypoints from Target cloud\n" << endl;
		pcl::copyPointCloud(result, *cloud_KeyPoints);
		target_Cloud_KeyPoints = cloud_KeyPoints;
		message += "Finished Target KeyPoint Capture\n";
		cloud_KeyPoints = nullptr;
	}
	else
	{
		cout << "Capturing Keypoints from Source cloud\n" << endl;
		pcl::copyPointCloud(result, *cloud_KeyPoints);
		source_Cloud_KeyPoints = cloud_KeyPoints;
		message += "Finished Source KeyPoint Capture\n";
		cloud_KeyPoints = nullptr;
	}*/
	tree = nullptr;
	sift = nullptr;

	std::cout << "No. of SIFT points in the result are " << result->points.size() << endl;
	result = nullptr;

	input_Cloud_KeyPoints = cloud_KeyPoints;
	cloud_KeyPoints = nullptr;
}



//Return keypoints
pcl::PointCloud<pcl::PointXYZRGB>::Ptr PointCloud_Features::getKeyPointCapture(){
	return input_Cloud_KeyPoints;
}



/**
Code based on tutorial from PCL 1.8 on Greedy Projection Trinagulation
Pointclouds.org. (2017). Documentation - Point Cloud Library (PCL). [online] 
Available at: http://pointclouds.org/documentation/tutorials/greedy_projection.php#greedy-triangulation 
[Accessed 1 Apr. 2017].

Generate Mesh for received pointcloud
	Kd-Tree is setup for Nearest Neighbour search in SIFT (Scale Invariant Feature Transform) 
		Setup GreedyProjectionTriangulation parameters for triangular mesh creation - Optimization required
			Execute triangulation to create Mesh
				Copy to PolygonMesh variable
**/
void PointCloud_Features::setMesh(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointcloud) {
	/*
	if (version == 1)
	{
		cout << "Generating Mesh for Target cloud" << endl;
		message += "Generating Mesh for Target cloud.\n";
	}
	else if (version == 2)
	{
		message += "Generating Mesh for Source cloud.\n";
		cout << "Generating Mesh for Source cloud" << endl;
	}*/
	// Create search tree*
	pcl::search::KdTree<pcl::PointXYZRGBNormal>::Ptr tree2(new pcl::search::KdTree<pcl::PointXYZRGBNormal>);
	tree2->setInputCloud(pointcloud);
	// Initialize objects
	pcl::GreedyProjectionTriangulation<pcl::PointXYZRGBNormal>::Ptr gp3(new pcl::GreedyProjectionTriangulation<pcl::PointXYZRGBNormal>);
	pcl::PolygonMesh::Ptr triangles(new pcl::PolygonMesh);

	// Set the maximum distance between connected points (maximum edge length)
	gp3->setSearchRadius(0.025);

	// Set typical values for the parameters - Use example standards for now -  Optimization required based of pointcloud
	gp3->setMu(2.5);
	gp3->setMaximumNearestNeighbors(100);
	gp3->setMaximumSurfaceAngle(M_PI / 4); // 45 degrees
	gp3->setMinimumAngle(M_PI / 18); // 10 degrees
	gp3->setMaximumAngle(2 * M_PI / 3); // 120 degrees
	gp3->setNormalConsistency(false);

	// Get result
	gp3->setInputCloud(pointcloud);
	gp3->setSearchMethod(tree2);
	gp3->reconstruct(*triangles);

	/* Additional vertex information
	vector<int> parts = gp3.getPartIDs();
	vector<int> states = gp3.getPointStates();*/

	/*
	Version	1.Target
	2.Source
	3.ICP
	*
	if (version == 1)
	{
		target_Triangles_Mesh = triangles;
		message += "Mesh created for Target cloud\n";
	}

	else if (version == 2)
	{
		source_Triangles_Mesh = triangles;
		//icp_Triangles_Mesh = source_Triangles_Mesh;
		message += "Mesh created for Source cloud\n";
	}

	else
		icp_Triangles_Mesh = triangles;*/

	input_Triangles_Mesh = triangles;
	triangles = nullptr;
	gp3 = nullptr;
	tree2 = nullptr;
}



//Return Mesh
pcl::PolygonMesh::Ptr PointCloud_Features::getMesh() {
	return input_Triangles_Mesh;
}