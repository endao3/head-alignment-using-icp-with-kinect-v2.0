#include "Controller_UI.h"

Controller_UI::Controller_UI() {}

/**
Initialize Controller
	Instantiate all variables required by controller to go through system
		Enter Main Menu
**/
void Controller_UI::init()
{
	interceptor = new LoggerInterceptor();
	pointCloudCreator = new PointCloud_Creator(interceptor);
	kdc_Data = new Kinect_DataCapture(interceptor);
	io_Operator = new IO_Operation(interceptor);
	icp_Operator = new Interactive_ICP(interceptor);

	mainMenu();
}



/**
	Get a Yes/No answer from User for a question
**/
void Controller_UI::checkUserInput(string question) {
	regex yesNo("['n'|'y']{1}");

	do {
		cout << "\n----------------------------------\n"
			 << question << 
			"\nY/N:> ";
		cin.clear();
		getline(cin, input);
		boost::algorithm::to_lower(input);
	} while (!regex_match(input, yesNo));
}


/**
**********************************************************************************************
									MAIN MENU SECTION
**********************************************************************************************
Menu for User to choose an option

1. KDC (Kinect Data Capture) is for live individual images from Kinect
	2. KDC + WriteToFile is for live individual images from Kinect and writing them to a file
		3. ICP starts the Iterative Closest Point sequence starting with its Menu
**/
int Controller_UI::mainMenu()
{
	regex mainMenuConstraints("[1-5]{1}");
	//Get choice from user
	do
	{
		cout << "\n------------------------------------"
				"\n	Main Menu"
				"\n------------------------------------"
				"\n1.KDC (Kinect Data Capture)"
				"\n2.KDC + WriteToFile"
				//"3.Load Image\n"
				"\n3.ICP"
				"\n4.Exit"
				"\n\nSelect an Option:> ";
		cin.clear();
		getline(cin, input);
		
	} while (!regex_match(input, mainMenuConstraints));
	selection = stoi(input);

	/**
		Go to iterations menu to choose how many images required
			Initialize Kinect
				Capture data and reduce iterator
					Return to Main menu
	**/
	if (selection == 1) {
		setIterations();
		if (iterator != 0)
		{
			kinectInit();
			while (iterator > 0)
			{
				kinectDataCapture(kinectOptions());
				iterator--;
			}
		}
		mainMenu();
	}

	/**
	Go to iterations menu to choose how many images required
		Initialize Kinect
			Capture data and reduce iterator
				Write data to file
					Return to Main menu
	**/
	else if (selection == 2) {
		setIterations();
		if (iterator != 0)
		{
			kinectInit();
			while (iterator > 0)
			{
				kinectDataCapture(kinectOptions());
				checkUserInput("Accept Image ?");

				if (input.compare("y") == 0)
					writeKDCToFile();

				iterator--;
			}
		}
		mainMenu();
	}

	/*Load image from file
	else if (selection == 3) {
		//loadImageFromFile(argc, argv);
		mainMenu();
	}*/

	/**
	Go to ICP Menu
		Return to Main menu
	**/
	else if (selection == 3) {
		ICP_Options();
		mainMenu();
	}

	//Exit to main.cpp
	else
		return 1;
}


/**
*********************************************************************************************
								KINECT SECTION
*********************************************************************************************
//Set number of frames to capture
**/

void Controller_UI::setIterations() {
	regex numberInt("^([0-9]+\d*|0)$");

	do {
		cout <<"\n---------------------------------------------" 
			   "\nIterations Menu (NOTE: Integer values only)"
			   "\n---------------------------------------------"
			   "\n Enter Number of Images to Capture."  
			   "\n Enter '0' to return to main menu"
			   "\n\nNumber:> ";
		cin.clear();
		getline(cin, input);
	} while (!regex_match(input, numberInt));

	iterator = stoi(input);
}


/**
Give menu of kinect data capture options
	Return a boolean based on answer
		1. Capture the whole area - false
			2. Capture the head only - true		
**/
bool Controller_UI::kinectOptions() {
	regex numberInt("[1-2]{1}");
	bool headCapture;

	do {
		cout << "\n------------------------------------"
			"\n	Kinect PointCloud Menu"
			"\n------------------------------------"
			"\n1. Full Picture"
			"\n2. Head Capture"
			"\n\nNumber:> ";
		cin.clear();
		getline(cin, input);
	} while (!regex_match(input, numberInt));


	switch (stoi(input)) {
	case 1: headCapture = false; break;
	case 2: headCapture = true; break;
	}

	return headCapture;
}



/**
Initialize Kinect Sensor
	Sleep for 1 second to allow Kinect to start up
**/
void Controller_UI::kinectInit() {
	kdc_Data->initSensor();
	_sleep(1000);//Wait for Kinect Sensor to turn on
}



/**
Capture Color Buffer
	Capture Depth Buffer
		Capture Body Track Frame
			Get Latest Frame	
				if Head selected, then wait for sensor to find head joint
					Create a Pointcloud from data captured
**/
void Controller_UI::kinectDataCapture(bool headCapture){
	kdc_Data->colorCapture(iterator);
	kdc_Data->depthCapture();
	kdc_Data->bodyCapture();
	kdc_Data->getLatestData();

	if (headCapture) {
	cout << "Sensor is searching for body joints for point cloud calibration."
			"\nPlease wait...\n" << endl;
	kdc_Data->getFacePos();
		//Keep capturing data until body points are captured
		while (kdc_Data->getFacePos() == 0) {
				kdc_Data->getLatestData();
			}
	}

	kdc_Data->createPointCloud(pointCloudCreator, headCapture);
}



/**
Confirm folder to write image to
	User must input a .pcd extension or error message is provided
		Write latest frame to a PCD/PLY file
**/
string Controller_UI::writeKDCToFile() {
	cin.clear();
	image_Folder = io_Operator->confirmFolderPath(image_Folder);

	input = "";
	while (input.find(".pcd") > 200) { //Greater than 200 because input value is high memory space value
		cout << "\n----------------------------------------"
			"\nPlease Enter Filename with pcd extension"
			"\nExample XYZfile.pcd"
			"\n----------------------------------------"
			"\nFilename:> ";
		cin.clear();
		getline(cin, input);
	}

	return io_Operator->writeImageToFile(image_Folder, input, *kdc_Data->getPointCloud());
}



/*Load image from file
void Controller_UI::loadImageFromFile(int argc, char* argv[]) {
	/*cv::Mat readMat = io_Operator->readImageFromFile(argc, argv);
	cv::imshow("Loaded Image", readMat);

	char c = cv::waitKey(0);
	while (false)
	{
		if (c == 'x')
			true;
	}
}*/




/**
**************************************************************************************
						ITERATIVE CLOSEST POINT SECTION
**************************************************************************************
Setup ICP Menu
	1.Load PCD data from a file
		2.Capture it from the Kinect directly
			 -> Start ICP process 
					3.Exit the program
						Return to Main Menu
**/
void Controller_UI::ICP_Options() {
	regex icpMenu("[1-3]{1}");
	bool finishICP = false;
	while (!finishICP)
	{
		do {
			cout << "\n\n------------------------------------"
				    "\n	ICP Menu"
					"\n------------------------------------"
					"\n1.Load Image"
					"\n2.Kinect Image"
					"\n3.Return to Main Menu"
					"\n\nSelect an Option:> ";
			cin.clear();
			getline(cin, input);

		} while (!regex_match(input, icpMenu));
		selection = stoi(input);


			//Start setting up Source Cloud for ICP from file
			if (selection == 1){
				cout << "\n---------------------------------"
					  "\nConfirm Source Cloud folder path."
					  "\n--------------------------------- " << endl;
				image_Folder = io_Operator->confirmFolderPath(image_Folder);//Confirm folder to load source pointcloud from

				cout << "\n-------------------------"
					<< "\nSelect Source Cloud file."
					<< "\n--------------------------" << endl;
				string sourceFile = io_Operator->getPCDFile(image_Folder);//Confirm PCD file to load source pointcloud from

				if (!sourceFile.compare("exit")==0) {//Didn`t choose to return to main menu
					cout << "\nLoading Source PCD File:" << sourceFile << endl;
					icp_Operator->loadPCDFile(true, sourceFile);
				}

				else {//Return to main menu on request
					finishICP = true;
					break;
				}
			}

			//Start capturing a Source Cloud for ICP from Kinect
			else if (selection == 2){
					bool acceptImage = false;
					kinectInit();
					//Continue getting new images until they accept one for ICP process
					while (!acceptImage){
						kinectDataCapture(kinectOptions());
						checkUserInput("Accept Image ?");
						if (input.compare("y") == 0){
							acceptImage = true;
							icp_Operator->loadPCDFile(true, writeKDCToFile());
						}
					}
			}
			
			//Else break loop and go back to main menu
			else {
				finishICP = true;
				break;
			}

			//Start setting up Target Cloud for ICP if no exit was requested
			cout << "\n---------------------------------"
				 << "\nConfirm Target Cloud folder path."
				 << "\n--------------------------------- " << endl;
			image_Folder = io_Operator->confirmFolderPath(image_Folder);//Confirm folder to load target pointcloud from

			cout << "\n-------------------------"
				 << "\nSelect Target Cloud file."
				 << "\n--------------------------" << endl;
			string targetFile = io_Operator->getPCDFile(image_Folder);//Confirm folder to load target pointcloud from

			if (!targetFile.compare("exit") == 0){ //Didn`t choose to return to main menu
					cout << "\nLoading Target PCD File:" << targetFile << endl;
					icp_Operator->loadPCDFile(false, targetFile);

					//Start ICP process with file from arguments and pointcloud from Kinect
					startICP();
					cin.clear();

					checkUserInput("\nContinue ICP ?");
						if (input.compare("n") == 0)
							finishICP = true;
			}
	}
}



/**
	Start ICP process 
		Visualize ICP in progress
**/
void Controller_UI::startICP(){
	//icp_Operator->loadPLYFile(filename);

	cout << "\nStarting ICP Process" << endl;
	icp_Operator->setupICP();
	cout << "\nICP Visualizer warming up..." << endl;
	icp_Operator->visualizeICP();
}



/**
*******************************************************************************************
							MEMORY CLEANUP SECTION
*******************************************************************************************
**/

//Clean up Controller_UI memory
void Controller_UI::cleanUpController(){
	delete io_Operator;
	delete kdc_Data;
	delete pointCloudCreator;
	//delete icp_Operator; //***HEAP MEMORY ERROR ON DELETION***
}



//Clean up Kinect_DataCapture memory
void Controller_UI::cleanUpSystem() {
	kdc_Data->shutdownSystem();
	interceptor->event_callback_2();
	delete interceptor;
}
