/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#pragma once
#ifndef POINTCLOUD_CREATOR_H
#define POINTCLOUD_CREATOR_H
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <Kinect.h>
#include "icontext.h"
#include "idispatcher.h"

class Interceptor;

#define SCR_HEIGHT_COLOR 1080
#define SCR_WIDTH_COLOR 1920
#define SCR_HEIGHT_DEPTH 424
#define SCR_WIDTH_DEPTH 512

#define DEPTH_LEVEL_MIN 500
#define DEPTH_LEVEL_MAX 1500

using namespace std;

class PointCloud_Creator 
{
private:
	float x_UpperLimit, x_LowerLimit, y_UpperLimit, y_LowerLimit;
	IContext* context;
	IDispatcher* dispatcher;
	string message;

protected:

public:
	PointCloud_Creator(Interceptor*);
	virtual ~PointCloud_Creator() {}
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloudInitialize(ICoordinateMapper*, vector<RGBQUAD>, vector<UINT16>,const CameraSpacePoint&, bool);
	void draw_cloud(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr);

};
#endif