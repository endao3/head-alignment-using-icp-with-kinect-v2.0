/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#pragma once
#ifndef LOGGER_H
#define LOGGER_H
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/local_time_adjustor.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>

using namespace std;
using namespace boost::posix_time;
using namespace boost::gregorian;

class Logger {
private:
	ofstream appendLogFile;

protected:

public:
	int initLog();
	void closeLogFile();
	int writeToLog(string);
	bool file_Check(string&);
};
#endif