/*
Name:		Enda O'Shea 
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
*/

#include "Controller_UI.h"

int main(int argc, char* argv[]){

	//Setup controller and start system.
	Controller_UI* control = new Controller_UI();
	control->init(); //Start program
	control->cleanUpSystem();//Clean up Kinect Memory
	control->cleanUpController();//Clean up Controller_UI memory
	delete control;
	
	return(0);
}
