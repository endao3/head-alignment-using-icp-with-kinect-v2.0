#include "IO_Operation.h"
#include "simplecontext.h"
#include "loggerdispatcher.h"

/*
Initialize required variables for Interceptor
*/
IO_Operation::IO_Operation(Interceptor* interceptor) 
{
	context = new SimpleContext();
	dispatcher = new LoggerDispatcher();

	dispatcher->registerInterceptor(interceptor);
	message = "";

}



/**
Get a Yes/No answer from User for a question
**/
void IO_Operation::checkUserInput(string question) {
	regex yesNo("['n'|'y']{1}");

	do {
		cout << "\n-----------------------------------------------------------------\n"
			<< question <<
			"\nY/N:> ";
		cin.clear();
		getline(cin, input);
		boost::algorithm::to_lower(input);
	} while (!regex_match(input, yesNo));
}



/*Read image from file
cv::Mat IO_Operation::readImageFromFile(int argc, char* argv[])
{
	message += "Reading Image Interceptor";

	for (int i = 0; i < argc; i++)
	{
		cout << argv[i] << endl;
	}
	//My Code
	if (argc > 0 && argc != 4)
	{
		message += "readImageFromFile :Must have 3 command line arguments.(Path,FileName,ExtensionType) \n";
		context->setValue(message);
		dispatcher->dispatch(context);
		exit(10);
	}

	const string &filename = argv[1];
	const string &image_Description = argv[2];
	const string &ext = argv[3];

	const string &image_Filename = filename + image_Description + ext;
	cv::Mat& read_Frame = cv::imread(image_Filename);
	

	if (read_Frame.empty())
	{
		//**********Change this to let user enter new image number
		message += ("readImageFromFile :No image matches " + image_Description + ext + " in the folder at path " + filename);
		context->setValue(message);
		dispatcher->dispatch(context);
		exit(10);
	}

	//Set the context with the string message
	context->setValue(message);
	//Pass the context with the message back to the dispatcher to be handled by the interceptor
	dispatcher->dispatch(context);
	//Clear the message variable for future use
	message = "";
	return read_Frame;

}*/



/**
Receives a folder name, image name and a pointcloud
	Checks if file already exists
		If no, writes PCD data to file
			Else error message is displayed and new filename must be chosen
				Dispatch messages to Interceptor
**/
string IO_Operation::writeImageToFile(string filename,string image_Name, pcl::PointCloud<pcl::PointXYZRGB> cloud)//cv::Mat& frame_Matrix)
{
	//My code
		message += ("Image Writer Process Starting.\nPoints:" + to_string(cloud.points.size()) + "\n");

		string new_Image = filename + image_Name;
		time_t now = time(0);
		while (file_Check(new_Image))
		{		
			cout << "\n************************************************************************"
				    "\nERROR: Filename " << image_Name << " already exists!"
				    "\n************************************************************************";

				image_Name = "";
				while(image_Name.find(".pcd") > 200){ //Greater than 200 because input value is high memory space value
				   cout <<  "\n----------------------------------------"
							"\nPlease Enter Filename with pcd extension"
							"\nExample XYZfile.pcd"
							"\n----------------------------------------"
						    "\nFilename:> ";
				    cin.clear();
					getline(cin, image_Name);
				}

			new_Image = filename + image_Name;
		}
		cout << "Writing " << image_Name << " to folder " << filename << endl;
		message += ("Writing " + image_Name + " to folder " + filename + "\n");
		time_t after = time(0);
		now = time(0);
		try
		{
			//PCD Binary
			//pcl::io::savePCDFile(new_Image, cloud, true);

			//PCD ASCII
			pcl::io::savePCDFileASCII(new_Image, cloud);

			//PLY ASCII
			//pcl::io::savePLYFileASCII(new_Image, cloud);
		}
		catch (exception ex)
		{
			message += ("WritingToFileException: ");
			message += ex.what();
			message += "\n";
			context->setValue(message);
			dispatcher->dispatch(context);
			exit(10);
		}
		after = time(0);
		message+= ("PCL IO System Time: " + to_string(after - now) + "secs");


		/*OpenCV Material to compress image in PNG format
		vector<int> compression_params;
		compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
		compression_params.push_back(9);

		cv::imwrite(new_Image, frame_Matrix, compression_params);*/
	
	/*catch (cv::Exception& ex) {
		fprintf(stderr, "Exception converting image to PNG format: %s\n", ex.what());
		system("pause");
		return 1;
	}*/
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
	return new_Image;
}



/**
Code from http://stackoverflow.com/questions/12774207/fastest-way-to-check-if-a-file-exist-using-standard-c-c11-c
Check if file exists
	Returns true if found
		Else returns false
**/
bool IO_Operation::file_Check(string& filename)
{
	struct stat buffer;
	return (stat(filename.c_str(), &buffer) == 0);
}



/*
Code from http://stackoverflow.com/questions/3010305/programmatically-selecting-file-in-explorer
NOT USED
void IO_Operation::BrowseToFile()
{
	/*
	BOOL WINAPI GetOpenFileName(
		_Inout_ LPOPENFILENAME lpofn
	);

	
/*
	//LPCTSTR file = filename;
	CString strArgs;
	strArgs = _T("/select,\"");
	//strArgs += filename.c_str();
	strArgs += filename;
	strArgs += _T("\"");

	ShellExecute(0, _T("open"), _T("explorer.exe"), strArgs, 0, SW_NORMAL);

	*/

	/* Displays an OpenFileDialog so the user can select a Cursor.  
	OpenFileDialog^ openFileDialog1 = new OpenFileDialog();
	openFileDialog1->Filter = "Cursor Files|*.cur";
	openFileDialog1->Title = "Select a Cursor File";

	// Show the Dialog.  
	// If the user clicked OK in the dialog and  
	// a .CUR file was selected, open it.  
	if (openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		// Assign the cursor in the Stream to  
		// the Form's Cursor property.  
		this->Cursor = gcnew
			System::Windows::Forms::Cursor(
				openFileDialog1->OpenFile());
	}
}*/



/**
Check with User if the FilePath is correct and the wish to progress
	If Yes, then finish
		Else, request new path name, add a '/' to the end if not provided and check if the folder exists
**/
string IO_Operation::confirmFolderPath(string image_Folder) {

	string question = "Current Path: " + image_Folder + "\n"
		+ "Do you want to keep this path ? ";
	checkUserInput(question);

	//Change folder extension if requested and ask question again to confirm.
	if (input.compare("n") == 0)
	{
		bool path = false;
		while (!path)
		{
			cout << "\n-----------------------------"
				    "\nPlease Enter new folder path."
					"\n-----------------------------"
					"\nPath:> ";
			cin.clear();
			getline(cin, input);
			//cout <<"Path ending:"<< boost::algorithm::ends_with(input, "\\") << endl;
			if (!boost::algorithm::ends_with(input, "\\"))
				input.push_back('\\');

			if (boost::filesystem::is_directory(input)) {
				image_Folder = input;
				path = true;
			}

			else
				cout << "\n**************************"
					    "\nERROR: Path not found."
					    "\n**************************" << endl;
		}
		confirmFolderPath(image_Folder);
	}
	return image_Folder;
}



/*
Check if PCD files exist in folder
	If Yes, progress to file selection
		Else, request new folder path
			When Yes, display list of all PCD files in folder and ask for user to select file
				Ensure user slects a PCD file, error message otherwise
					Return PCD file
*/
string IO_Operation::getPCDFile(string image_Folder) {
	
	listFiles(image_Folder);
	//Check if pcd files were found in folder, change folder if none found.
	while (amountPCDFiles == 0) {
		cout <<  "\n*********************************************"
				 "\nERROR: No PCD files found in this folder !!!"
				 "\n       Please select a new Path."
				 "\n*********************************************" << endl;
			
		image_Folder = confirmFolderPath(image_Folder);
		listFiles(image_Folder);
		//getPCDFile(image_Folder);
	}
		
		cout << "\nPCD File Options"
			"\n------------------------------------------------"
			"\nEnter 'Exit' to return to main menu."
			"\nEnter PCD Image filename with extension for ICP."
			"\n------------------------------------------------"
			"\nFilename:> ";
		cin.clear();
		getline(cin, input);
	


		//If user entered exit, return exit string.
		string exit = input;
		boost::algorithm::to_lower(exit);
		if (exit.compare("exit") == 0)
			return exit;

		//Folder to load pcd files
		string pcdFile = (image_Folder + input);
		while (!file_Check(pcdFile) || pcdFile.find(".pcd") > 200)
		{
			cout <<"PCDFile" << pcdFile.find(".pcd") << endl;

			cout <<  "\n***************************************************************"
					 "\nERROR: "<< pcdFile << " does not exist or file type is invalid."
					 "\nExample: fileXYZ.pcd\n"
					 "\n***************************************************************"
					 "\n\n------------------------------------------------"
					 "\nEnter 'Exit' to return to main menu."
					 "\nEnter PCD Image filename with extension for ICP."
					 "\n------------------------------------------------"
					 "\nFilename:> ";
			cin.clear();
			getline(cin, input);

			//If user entered exit, return exit string.
			exit = input;
			boost::algorithm::to_lower(exit);
			if (exit.compare("exit") == 0)
				return exit;

			pcdFile = (image_Folder + input);
		}
	return pcdFile;
}



/*
List all files in folder that end in .pcd
	Increment counter for every file found, if counter=0 then it requires a new folder selection
*/
void IO_Operation::listFiles(string image_Folder) {
	/*Segment refactored for scanning directory using boost https://gist.github.com/vivithemage/9517678 by belalmoh
	Lists all files in folder with .pcd extension
	*/
	boost::filesystem::path targetDir(image_Folder);
	boost::filesystem::recursive_directory_iterator iter(image_Folder), eod;
	amountPCDFiles = 0;

	BOOST_FOREACH(boost::filesystem::path const& i, make_pair(iter, eod)) {
		if (is_regular_file(i)) {
			if (i.filename().string().find(".pcd") < 200){//200 selected because it is longer than 99% of filenames and less than any empty memory
				amountPCDFiles++;
				cout << i.filename().string() << endl;
			}
		}
	}
}
