#include "PointCloud_Creator.h"
#include "simplecontext.h"
#include "loggerdispatcher.h"

/*
Initialize required variables for Interceptor 
*/
PointCloud_Creator::PointCloud_Creator(Interceptor* interceptor){
	context = new SimpleContext();
	dispatcher = new LoggerDispatcher();

	dispatcher->registerInterceptor(interceptor);
	message = "";
}



/*
Code from https://gist.github.com/UnaNancyOwen/8f74fe7f4ee91827734d Refactored for this project
Reference
-------- -
Mvp.microsoft.com. (2017).Tsukasa Sugiura.[online] Available at :
https://mvp.microsoft.com/en-us/PublicProfile/5000954?fullName=Tsukasa%20Sugiura
[Accessed 31 Mar. 2017].

/**
Receieve a CoOridinate Mapper, two buffers and a boolean
	If boolean is True, then set up Head Capture constraints for partial pointcloud
		Else create complete pointcloud
			Align both pointclouds using CoOrdinate Mapper
				Loads depth and colour data
					Based on constraints add data to pointcloud
						Return pointcloud
							Dispatch messages to Logfile interceptor
**/
pcl::PointCloud<pcl::PointXYZRGB>::Ptr PointCloud_Creator::pointCloudInitialize(ICoordinateMapper* pCoordinateMapper, vector<RGBQUAD> colorBuffer, vector<UINT16> depthBuffer, const CameraSpacePoint& facePos, bool headCapture)
{
	if (headCapture) {
		//Square Scan Area for Head
		x_UpperLimit = facePos.X + 0.125;
		x_LowerLimit = facePos.X - 0.125;
		y_LowerLimit = facePos.Y - 0.125;
		y_UpperLimit = facePos.Y + 0.125;
	}
	
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud(new pcl::PointCloud<pcl::PointXYZRGB>());
	pointcloud->width = static_cast<uint32_t>(SCR_WIDTH_DEPTH);
	pointcloud->height = static_cast<uint32_t>(SCR_HEIGHT_DEPTH);
	pointcloud->is_dense = false;

	message += ("Point Cloud size: Width = " + to_string(pointcloud->width) + ": Height = " + to_string(pointcloud->height)+"\n");
	message += ("Screen Size: "+ to_string(SCR_WIDTH_DEPTH) + ":" + to_string(SCR_HEIGHT_DEPTH)+"\n");

	//Camera Capture settings
	for (int y = 0; y < SCR_HEIGHT_DEPTH; y = y + 1) {
		for (int x = 0; x < SCR_WIDTH_DEPTH; x = x + 1) {
			pcl::PointXYZRGB point;
			//cout << "PC Test 3" << endl;
			DepthSpacePoint depthSpacePoint = { static_cast<float>(x), static_cast<float>(y) };
			UINT16 depth = depthBuffer[y * SCR_WIDTH_DEPTH + x];
			//cout << "PC Test 4" << endl;
			// Coordinate Mapping Depth to Color Space, and Setting PointCloud RGB
			ColorSpacePoint colorSpacePoint = { 0.0f, 0.0f };
			pCoordinateMapper->MapDepthPointToColorSpace(depthSpacePoint, depth, &colorSpacePoint);
			int colorX = static_cast<int>(std::floor(colorSpacePoint.X + 0.5f));
			int colorY = static_cast<int>(std::floor(colorSpacePoint.Y + 0.5f));
			//cout << "PC Test 5" << endl;
			if ((0 <= colorX) && (colorX < SCR_WIDTH_COLOR) && (0 <= colorY) && (colorY < SCR_HEIGHT_COLOR)) {
				RGBQUAD color = colorBuffer[colorY * SCR_WIDTH_COLOR + colorX];
				point.b = color.rgbBlue;
				point.g = color.rgbGreen;
				point.r = color.rgbRed;
			}
			//cout << "PC Test 6" << endl;
			// Coordinate Mapping Depth to Camera Space, and Setting PointCloud XYZ
			CameraSpacePoint cameraSpacePoint = { 0.0f, 0.0f, 0.0f };
			pCoordinateMapper->MapDepthPointToCameraSpace(depthSpacePoint, depth, &cameraSpacePoint);
			if ((0 <= colorX) && (colorX < SCR_WIDTH_COLOR) && (0 <= colorY) && (colorY < SCR_HEIGHT_COLOR)) {
				point.x = cameraSpacePoint.X;
				point.y = cameraSpacePoint.Y;
				point.z = cameraSpacePoint.Z;
			}
			//cout << "PC Test 7" << endl;
			if (headCapture) {
				//Depth constraints
				if (depth > DEPTH_LEVEL_MIN && depth < DEPTH_LEVEL_MAX)
					//Head Point Parameters: Points only inside scan area
					if (point.y <= y_UpperLimit && point.y >= y_LowerLimit && point.x >= x_LowerLimit && point.x <= x_UpperLimit)
						pointcloud->push_back(point);
			}
			else
				pointcloud->push_back(point);
			}
		}
	
	if (headCapture) {
		message += ("Depth Cloud created based on Depth constraints (Minimum Level:" + to_string(DEPTH_LEVEL_MIN)
			+ " - Maximum Level:" + to_string(DEPTH_LEVEL_MAX) + ") and/or Head point parameters. (Head Posisition : "
			+ to_string(facePos.X) + " : " + to_string(facePos.Y) + ")\n");
	}

	else
		message += "Full Picture Depth Cloud created.\n";
	
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
	return pointcloud;
}



/*
Receieve a Point cloud object with XTZ and RGB properties
	Starts visualizer
		Loads cloud
			Dispatch messages to Logfile interceptor
*/
void PointCloud_Creator::draw_cloud(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud)
{
	pcl::visualization::CloudViewer viewer("Point Cloud Viewer");
	message+=("Cloud Viewer Data: " + to_string(cloud->size())+"\n");
	viewer.showCloud(cloud);
	while (!viewer.wasStopped()) {
		// Input Key ( F1 key to Exit )
		if (GetKeyState(VK_F1) < 0)
			break;
	}
	context->setValue(message);
	dispatcher->dispatch(context);
	message = "";
}
