/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#pragma once
#ifndef INTERCEPTOR_H
#define INTERCEPTOR_H

class IContext;

//Interface for Interceptor Objects
class Interceptor {
public:
    virtual void event_callback_1(IContext*) = 0;
    virtual void event_callback_2() = 0;
};

#endif // INTERCEPTOR_H
