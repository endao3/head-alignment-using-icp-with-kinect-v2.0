#include "Logger.h"

/*
code from http://stackoverflow.com/questions/23967697/if-file-exist-work-with-it-if-no-create-it
	Open log file
		Append string data to file
			Append time/date to file
				If file does not exist, creates a file
*/
int Logger::initLog()
{
	const string filename = "G:\\Kinect_Data\\Logs\\log.txt";
	try
	{
		appendLogFile.open(filename, std::fstream::in | std::fstream::out | std::fstream::app);
		// If file does not exist, Create new file
		if (!appendLogFile)
		{
			cout << "Cannot open file, file does not exist. Creating new file: ";
			appendLogFile.open(filename, fstream::in | fstream::out | fstream::trunc);
		}
	appendLogFile << "\n***************************************************************\n";
	ptime todayUtc(day_clock::universal_day(), second_clock::universal_time().time_of_day());
	appendLogFile << "\n" << to_simple_string(todayUtc) << "\n";
	}
	catch (exception e) {
		cout << "Error loading log file: " << e.what() << endl;
		return -1;
	}
	return 0;
}

/**
Write messages to log from interceptor
**/
int Logger::writeToLog(string message){
	try {
		appendLogFile << message << "\n";
	}
	catch (exception e) {
		cout << "Error loading log file: " << e.what() << endl;
		return -1;
	}
	return 0;
}

/**
Code from http://stackoverflow.com/questions/12774207/fastest-way-to-check-if-a-file-exist-using-standard-c-c11-c
	Check if file exists
		Returns true if found
			Else returns false
**/
bool Logger::file_Check(string& filename)
{
	struct stat buffer;
	return (stat(filename.c_str(), &buffer) == 0);
}

/**
Close the logfile
**/
void Logger::closeLogFile() {
	if(appendLogFile.is_open())
		appendLogFile.close();
}
