/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#pragma once
#ifndef INTERACTIVE_ICP_H
#define INTERACTIVE_ICP_H
#include <iostream>
#include <string>

#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/time.h>  
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/pfh.h>
#include <pcl/surface/gp3.h>
#include "icontext.h"
#include "idispatcher.h"
#include "PointCloud_Features.h"


#define PI 3.14159265
class Interceptor;

using namespace std;

class Interactive_ICP
{
private:
	int iterations;
	double yaw, pitch, roll;
	double angle;
	string message, headPos, iterations_cnt, input, icp_status;
	bool fileCloud = false;
	bool next_iteration = false;
	pcl::console::TicToc timer;

	Eigen::Affine3d rotation_matrix;
	Eigen::Matrix4d transformation_matrix;
	Eigen::Vector4d target_Centroid;
	Eigen::Vector4d source_Centroid;

	pcl::PolygonMesh::Ptr source_Triangles_Mesh;
	pcl::PolygonMesh::Ptr icp_Triangles_Mesh;
	pcl::PolygonMesh::Ptr target_Triangles_Mesh;

	pcl::IterativeClosestPoint<pcl::PointXYZRGBNormal, pcl::PointXYZRGBNormal> icp;
	
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr icp_Cloud_Normals;

	PointCloud_Features* target_Features;
	PointCloud_Features* source_Features;
	PointCloud_Features* icp_Features;

	IContext* context;
	IDispatcher* dispatcher;


	//pcl::visualization::PCLVisualizer viewer;
	//ICP PointXYZRGB 
	/*pcl::PointCloud<pcl::PointXYZRGB>::Ptr icp_cloud; //Cloud to transform
	pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;*/
	//ICP PointXYZRGBNormal
	//pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_Cloud_KeyPoints; 
	//pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr source_Cloud_KeyPoints_Normals;
	//pcl::PointCloud<pcl::PointXYZRGB>::Ptr target_Cloud_KeyPoints; 
	//pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr target_Cloud_KeyPoints_Normals;
	//pcl::NormalEstimation<pcl::PointXYZRGB, pcl::PointXYZRGBNormal> normals;
	//pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr target_Cloud_Normals;
	//pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr source_Cloud_Normals;

	

protected:

public:
	Interactive_ICP(Interceptor*);
	virtual ~Interactive_ICP() {}
	void print4x4Matrix(const Eigen::Matrix4d &);
	//int loadPLYFile(string);
	int loadPCDFile(bool, string);
	int setupICP();
	int visualizeICP();
	void getAngle();
	void getAngleOfRotation();
	void setCentroids();

	/*void runOnce(const pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBNormal>);
	void autoRun(int, const pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGBNormal>);
	void stopRun();
	void generateMesh();
	void computeCloudNormals(pcl::PointCloud<pcl::PointXYZRGB>::Ptr, bool);
	void keyPointCapture(pcl::PointCloud<pcl::PointXYZRGB>::Ptr, bool);
	//void calculateHistogram(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr, bool);
	void generateMesh(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr, int);*/
};
#endif