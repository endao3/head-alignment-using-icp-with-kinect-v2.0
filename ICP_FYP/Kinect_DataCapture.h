/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#pragma once
#ifndef KINECT_DATACAPTURE__H
#define KINECT_DATACAPTURE_H
#define NOMINMAX

#include "PointCloud_Creator.h"
#include <stdlib.h>
#include <Windows.h>
#include <Kinect.h>
#include <Kinect.Face.h>
#include "opencv2\opencv.hpp"
#include <pcl/visualization/cloud_viewer.h>
#include "icontext.h"
#include "idispatcher.h"

class Interceptor;

using namespace std;

/*
SafeRelease Code from video tutorial https://www.youtube.com/watch?v=L1Kgm4S8c90
	Release Pointer safely
*/
template<typename T>
void SafeRelease(T& ptr) { if (ptr) { ptr->Release(); ptr = nullptr; } }

class Kinect_DataCapture
{
private:
	
	HRESULT hResult;
	string message;
	IContext* context;
	IDispatcher* dispatcher;
	vector<RGBQUAD> colorBuffer;
	vector<UINT16> depthBuffer;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud;
	

	IKinectSensor* pSensor = nullptr;
	ICoordinateMapper* pCoordinateMapper = nullptr;

	IColorFrameSource* pColorSource = nullptr;
	IDepthFrameSource* pDepthSource = nullptr;
	IBodyFrameSource* pBodySource = nullptr;

	IColorFrameReader* pColorReader = nullptr;
	IDepthFrameReader* pDepthReader = nullptr;
	IBodyFrameReader* pBodyReader = nullptr;

	IBodyFrame* pBodyFrame = nullptr;
	IColorFrame* pColorFrame = nullptr;
	IDepthFrame* pDepthFrame = nullptr;

	IBody* bodyReader = nullptr;
	CameraSpacePoint* faceSpacePoint;

	

protected:


public:
	Kinect_DataCapture(Interceptor*);
	virtual ~Kinect_DataCapture(){}
	int initSensor();
	int colorCapture(int);
	int depthCapture();
	int bodyCapture();
	int getLatestData();
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr getPointCloud();
	void processBodies(const unsigned int &, IBody **);
	//double getNeckPos();
	double getFacePos();
	void createPointCloud(PointCloud_Creator*, bool);
	void shutdownSystem();

};
#endif