/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#pragma once
#ifndef IO_OPERATION_H
#define IO_OPERATION_H
#include "Logger.h"
#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <sys/stat.h>
#include <atlstr.h>

#include <Windows.h>
#include <Commdlg.h>
#include <tchar.h>
#include <regex>

#include "opencv2\opencv.hpp"
#include "icontext.h"
#include "idispatcher.h"

class Interceptor;
using namespace std;

class IO_Operation
{
private:
	IContext* context;
	IDispatcher* dispatcher;
	string message, input, folderPath;
	int amountPCDFiles;

protected:

public:
	IO_Operation(Interceptor*);
	virtual ~IO_Operation() {}
	bool file_Check(string&);
	//cv::Mat readImageFromFile(int, char*[]);
	//void BrowseToFile();
	string writeImageToFile(string, string, pcl::PointCloud<pcl::PointXYZRGB>);
	void checkUserInput(string);
	string confirmFolderPath(string);
	string getPCDFile(string);
	void listFiles(string);
};
#endif