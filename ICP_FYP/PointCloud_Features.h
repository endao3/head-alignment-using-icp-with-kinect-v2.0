/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#pragma once
#ifndef POINTCLOUD_FEATURES_H
#define POINTCLOUD_FEATURES_H
#include <iostream>
#include <string>

#include <pcl/point_types.h>
#include <pcl/registration/icp.h> 
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/pfh.h>
#include <pcl/surface/gp3.h>

using namespace std;

class PointCloud_Features
{
private:
	string name;
	Eigen::Vector4d *input_Centroid;

	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr input_Cloud_Normals;
	pcl::NormalEstimation<pcl::PointXYZRGB, pcl::PointXYZRGBNormal> normals;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr input_Cloud_KeyPoints;
	pcl::PolygonMesh::Ptr input_Triangles_Mesh;
	
	
	
	/*
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr source_Cloud_KeyPoints;
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr source_Cloud_KeyPoints_Normals;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr target_Cloud_KeyPoints;
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr target_Cloud_KeyPoints_Normals;

	/*pcl::PolygonMesh::Ptr source_Triangles_Mesh;
	pcl::PolygonMesh::Ptr icp_Triangles_Mesh;
	pcl::PolygonMesh::Ptr target_Triangles_Mesh;*/

	
	/*pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr source_Cloud_Normals;
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr icp_Cloud_Normals;*/

protected:

public:
	PointCloud_Features(string);
	PointCloud_Features(string, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr);
	virtual ~PointCloud_Features() {}
	
	string getCloudName();

	/*Eigen::Vector4d* getCentroid();
	void setCentroid(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr);*/

	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr getCloudNormals();
	void setCloudNormals(pcl::PointCloud<pcl::PointXYZRGB>::Ptr);

	pcl::PointCloud<pcl::PointXYZRGB>::Ptr getKeyPointCapture();
	void setKeyPointCapture(pcl::PointCloud<pcl::PointXYZRGB>::Ptr);

	pcl::PolygonMesh::Ptr getMesh();
	void setMesh(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr);
};


#endif;