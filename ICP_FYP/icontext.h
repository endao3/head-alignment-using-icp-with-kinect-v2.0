/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#pragma once
#ifndef ICONTEXT_H
#define ICONTEXT_H
#include <string>

using namespace std;

//Interface for Context Objects
class IContext {
public:
    virtual string getValue() = 0;
    virtual void setValue(string) = 0;
};

#endif // ICONTEXT_H
