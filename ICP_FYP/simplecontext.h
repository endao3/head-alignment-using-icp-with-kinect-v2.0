#ifndef SIMPLECONTEXT_H
#define SIMPLECONTEXT_H
#include "icontext.h"

class SimpleContext: public IContext {
private:
	string value;

public:
    virtual string getValue();
    virtual void setValue(string);

};

#endif // SIMPLECONTEXT_H
