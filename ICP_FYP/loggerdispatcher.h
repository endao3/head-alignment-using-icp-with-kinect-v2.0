/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#ifndef LOGGERDISPATCHER_H
#define LOGGERDISPATCHER_H
#include <list>
#include "idispatcher.h"

using namespace std;

class IContext;
class Interceptor;

class LoggerDispatcher: public IDispatcher {
private:
	list<Interceptor* > interceptors;

public:
    virtual void dispatch(IContext*);
    virtual void registerInterceptor(Interceptor*);
    virtual void removeInterceptor(Interceptor*);
};

#endif // LOGGERDISPATCHER_H
