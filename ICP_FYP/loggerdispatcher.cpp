#include "loggerdispatcher.h"
#include "interceptor.h"


//Go through all Interceptors and execute event_callback methods
void LoggerDispatcher::dispatch(IContext* context)
{
    for(std::list<Interceptor *>::iterator i = this->interceptors.begin(); i != this->interceptors.end(); ++i)
    {
        (*i)->event_callback_1(context);
		(*i)->event_callback_2();
    }
}

//Register interceptor
void LoggerDispatcher::registerInterceptor(Interceptor* i){
    interceptors.push_back(i);
}

//Remove interceptor
void LoggerDispatcher::removeInterceptor(Interceptor* i){
    interceptors.remove(i);
}
