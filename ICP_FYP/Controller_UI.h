/*
Name:		Enda O'Shea
ID:			13062344
Course:		LM051
Supervisor: Tiziana Margaria
Year:		2016/17
*/
#pragma once
#ifndef CONTROLLER_UI_H
#define CONTROLLER_UI_H
#include "Kinect_DataCapture.h"
#include "PointCloud_Creator.h"
#include "IO_Operation.h"
#include "Interactive_ICP.h"
#include "PointCloud_Features.h"
#include <stdlib.h>
#include "interceptor.h"
#include "loggerinterceptor.h"

#include <regex>
#include <algorithm>
#include "boost/filesystem.hpp"

using namespace std;

class Controller_UI
{
private:
	string image_Folder = "G:\\Kinect_Data\\Heads\\FullPoint\\";

	string input;
	int selection, iterator;
	bool acceptImage;

	Interceptor* interceptor;

	IO_Operation* io_Operator;
	Kinect_DataCapture* kdc_Data;
	Interactive_ICP* icp_Operator;
	PointCloud_Creator* pointCloudCreator;

	
	

protected:

public:
	Controller_UI();
	virtual ~Controller_UI() {}

	void init();

	int mainMenu();
	void checkUserInput(string);

	void kinectInit();
	void kinectDataCapture(bool);
	bool kinectOptions();
	void setIterations();
	//void loadImageFromFile(char*[]);
	
	string writeKDCToFile();

	void ICP_Options();
	void startICP();

	void cleanUpSystem();
	void cleanUpController();
};
#endif